/*
 * File: /Users/thibautmorant/Desktop/UPSSITECH/M1/S1/TER/PROJECT/ter/HEXAPOD/src/main.cpp
 * Created Date: Tuesday November 27th 2018
 * Author: MORANT Thibaut
 * 
 * -----
 * 
 * Last Modified: Wed Nov 28 2018
 * Modified By: MORANT Thibaut
 * 
 * -----
 * 
 * HEXAPOD
 * Copyright (C) 2018  MORANT Thibaut
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * -----
 * 
 * HISTORY:
 */


#include <iostream>
#include <string>
#include <thread>

#include "../include/Structure/Hexapod.h"
#include "../include/Controller/Controller.h"
#include "../include/Tools/DynamixelAPI.h"
#include "../include/Tools/Kinematic.h"

using namespace std;

float temp(float value) {
	return ((value*(-500)) / 150) + 500;
}

void hexapodTest() {
	cout << "--- TEST HEXAPOD ---" << endl;
	Hexapod *hexapod(0);

	hexapod = new Hexapod();
	cout << hexapod->toString() << endl;

	delete hexapod;
	hexapod = 0;
}

void ControllerTest() {
	cout << "--- TEST CONTROLLER ---" << endl;
	Controller *controller(0);
	Hexapod *hexapod(0);

	hexapod = new Hexapod();
	controller = new Controller("/dev/input/js0");
	controller->Start(hexapod);

	delete hexapod;
	delete controller;

	hexapod = 0;
	controller = 0;
}

int APITest() {
	if (dynamixelAPI.start_UART() < 0)
		return 1;

	dynamixelAPI.move(1, temp(0));
	dynamixelAPI.move(2, temp(0));
	dynamixelAPI.move(3, temp(0));
	dynamixelAPI.move(4, temp(0));
	dynamixelAPI.move(5, temp(0));
	dynamixelAPI.move(6, temp(0));

	dynamixelAPI.move(11, -temp(30));
	dynamixelAPI.move(12, -temp(30));
	dynamixelAPI.move(13, -temp(30));
	dynamixelAPI.move(14, -temp(30));
	dynamixelAPI.move(15, -temp(30));
	dynamixelAPI.move(16, -temp(30));

	dynamixelAPI.move(21, temp(-40));
	dynamixelAPI.move(22, temp(-40));
	dynamixelAPI.move(23, temp(-40));
	dynamixelAPI.move(24, temp(-40));
	dynamixelAPI.move(25, temp(-40));
	dynamixelAPI.move(26, temp(-40));

	dynamixelAPI.stop_UART();

	return 0;

}

int main() {
	hexapodTest();

	// ControllerTest();

	APITest();

	float *value = kinematic.directKinematic(0, -30, -40, 0, 0, 90, 70, 150, 155);
	cout << value[0] << endl;
	cout << value[1] << endl;
	cout << value[2] << endl;

	float *value2 = kinematic.inverseKinematic(value[0], value[1], value[2], 0, 0, 90, 70, 150, 155);
	cout << value2[0] << endl;
	cout << value2[1] << endl;
	cout << value2[2] << endl;


	return 0;
}