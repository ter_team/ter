/*
 * File: /Users/thibautmorant/Desktop/UPSSITECH/M1/S1/TER/PROJECT/ter/HEXAPOD/src/Tools/Kinematic.cpp
 * Created Date: Tuesday November 27th 2018
 * Author: MORANT Thibaut
 * 
 * -----
 * 
 * Last Modified: Wed Nov 28 2018
 * Modified By: MORANT Thibaut
 * 
 * -----
 * 
 * HEXAPOD
 * Copyright (C) 2018  MORANT Thibaut
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * -----
 * 
 * HISTORY:
 */


#include "../../include/Tools/Kinematic.h"

using namespace std;


float Kinematic::alKashi(float a, float b, float c) {

	return -(acos(((a * a) + (b * b) - (c * c)) / (2 * a * b)));
}

float *Kinematic::directKinematic(float theta1, float theta2, float theta3, float thetaCorrection_1, float thetaCorrection_2, float thetaCorrection_3, float legPart_1, float legPart_2, float legPart_3) {
	float t1((theta1 - thetaCorrection_1) * M_PI / 180.0);
	float t2((theta2 - thetaCorrection_2) * M_PI / 180.0);
	float t3(-(theta3 - thetaCorrection_3) * M_PI / 180.0);

	float planContribution(legPart_1 + legPart_2 * cos(t2) + legPart_3 * cos(t2 + t3));

	float *coordinates = new float[3];

	coordinates[0] = cos(t1) * planContribution;
	coordinates[1] = sin(t1) * planContribution;
	coordinates[2] = -(legPart_2 * sin(t2) + legPart_3 * sin(t2 + t3));


	return coordinates;
}

float *Kinematic::inverseKinematic(float x, float y, float z, float thetaCorrection_1, float thetaCorrection_2, float thetaCorrection_3, float legPart_1, float legPart_2, float legPart_3) {
	float t1(atan2(y, x));
	float t2, t3;
	float xp(sqrt(pow(x, 2) + pow(y, 2)) - legPart_1);

	if(xp < 0) {
		cout << "Destination point too close" << endl;
		xp = 0;
	}

	float d(sqrt(pow(xp, 2) + pow(z, 2)));

	if(d > (legPart_2 + legPart_3)) {
		cout << "Destination point too far away" << endl;
		d = legPart_2 + legPart_3;
	}


	try {
		t2 = alKashi(legPart_2, d, legPart_3) - atan2(z, xp);
		t3 = M_PI - alKashi(legPart_2, legPart_3, d);
	} catch(const exception & e) {
		cout << "Math error" << endl;
	}

	float *angles = new float[3];

	angles[0] = modulo180(t1 * (180 / M_PI));
	angles[1] = modulo180(t2 * (180 / M_PI) + thetaCorrection_2);
	angles[2] = modulo180(t3 * (180 / M_PI) + thetaCorrection_3);


	return angles;
}

float Kinematic::modulo180(float angle) {
	if((-180 < angle) and (angle < 180))
		return angle;

	if(((int)angle % 360) > 180)
		return -360 + ((int)angle % 360);


	return angle;
}