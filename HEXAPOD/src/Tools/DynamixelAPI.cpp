/*
 * File: /Users/thibautmorant/Desktop/UPSSITECH/M1/S1/TER/PROJECT/ter/HEXAPOD/src/Tools/DynamixelAPI.cpp
 * Created Date: Tuesday November 27th 2018
 * Author: MORANT Thibaut
 * 
 * -----
 * 
 * Last Modified: Wed Nov 28 2018
 * Modified By: MORANT Thibaut
 * 
 * -----
 * 
 * HEXAPOD
 * Copyright (C) 2018  MORANT Thibaut
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * -----
 * 
 * HISTORY:
 */


#include "../../include/Tools/DynamixelAPI.h"

// Macro for the selection of the Serial Port

#define availableData(args)		(serialDataAvail (args))		// Check Serial Data Available
#define peekData()				(peek())						// Peek Serial Data

// Macro for Timing

#define delayus(args)			(sleep(args))					// Delay Microseconds

// Macro for Comunication Flow Control
#define setDPin(DirPin,Mode)	(pinMode(DirPin,Mode))			// Select the Switch to TX/RX Mode Pin
#define switchCom(DirPin,Mode)	(digitalWrite(DirPin,Mode))		// Switch to TX/RX Mode


using namespace std;


int DynamixelAPI::start_UART() {
	wiringPiSetup();

	Direction_Pin = 1;
	setDPin(Direction_Pin,OUTPUT);

	string portname = "/dev/serial0";

	fd = open(portname.c_str(), O_RDWR | O_NOCTTY | O_SYNC);
	if(fd < 0) {
		cout << "Error opening " << portname << endl;

		return -1;
	}

	/*baudrate 115200, 8 bits, no parity, 1 stop bit */
	int speed = B1000000;
	struct termios tty;

	if(tcgetattr(fd, &tty) < 0) {
		cout << "Error from tcgetattr." << endl;

		return -1;
	}

	cfsetospeed(&tty, (speed_t)speed);
	cfsetispeed(&tty, (speed_t)speed);

	tty.c_cflag |= (CLOCAL | CREAD);	/* ignore modem controls */
	tty.c_cflag &= ~CSIZE;
	tty.c_cflag |= CS8;					/* 8-bit characters */
	tty.c_cflag &= ~PARENB;				/* no parity bit */
	tty.c_cflag &= ~CSTOPB;				/* only need 1 stop bit */
	tty.c_cflag &= ~CRTSCTS;			/* no hardware flowcontrol */

	/* setup for non-canonical mode */
	tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
	tty.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
	tty.c_oflag &= ~OPOST;

	/* fetch bytes as they become available */
	tty.c_cc[VMIN] = 1;
	tty.c_cc[VTIME] = 1;

	if(tcsetattr(fd, TCSANOW, &tty) != 0) {
		cout << "Error from tcgetattr." << endl;

		return -1;
	}

	return 0;
}

void DynamixelAPI::stop_UART() {
	close(fd);
}

int DynamixelAPI::read_error(void) {
	Time_Counter = 0;

	while((availableData(fd) > 0) && (Time_Counter < TIME_OUT)) {
		Incoming_Byte1 = readData();
		Incoming_Byte2 = readData();
		if((Incoming_Byte1 == 255) && (Incoming_Byte2 == 255)) {
			readData();
			readData();
			readData();
			Error_Byte = readData();

			return (Error_Byte);
		}

		Time_Counter++;
	}

	return (-1);
}

int DynamixelAPI::reset(unsigned char ID) {
	Checksum = (~(ID + AX_RESET_LENGTH + AX_RESET))&0xFF;

	switchCom(Direction_Pin,Tx_MODE);
	sendData(AX_START);
	sendData(AX_START);
	sendData(ID);
	sendData(AX_RESET_LENGTH);
	sendData(AX_RESET);
	sendData(Checksum);
	delayus(TX_DELAY_TIME);
	switchCom(Direction_Pin, Rx_MODE);
	delayus(RX_DELAY_TIME);

	return (read_error());
}


int DynamixelAPI::ping(unsigned char ID) {
	Checksum = (~(ID + AX_READ_DATA + AX_PING))&0xFF;

	switchCom(Direction_Pin,Tx_MODE);
	sendData(AX_START);
	sendData(AX_START);
	sendData(ID);
	sendData(AX_READ_DATA);
	sendData(AX_PING);
	sendData(Checksum);
	delayus(TX_DELAY_TIME);
	switchCom(Direction_Pin, Rx_MODE);
	delayus(RX_DELAY_TIME);

	return (read_error());
}

int DynamixelAPI::setID(unsigned char ID, unsigned char newID) {    
	Checksum = (~(ID + AX_ID_LENGTH + AX_WRITE_DATA + AX_ID + newID))&0xFF;

	switchCom(Direction_Pin,Tx_MODE);
	sendData(AX_START);
	sendData(AX_START);
	sendData(ID);
	sendData(AX_ID_LENGTH);
	sendData(AX_WRITE_DATA);
	sendData(AX_ID);
	sendData(newID);
	sendData(Checksum);
	delayus(TX_DELAY_TIME);
	switchCom(Direction_Pin,Rx_MODE);
	delayus(RX_DELAY_TIME);

	return (read_error());
}

int DynamixelAPI::setBD(unsigned char ID, long baud) {
	unsigned char Baud_Rate = (2000000/baud) - 1;
	Checksum = (~(ID + AX_BD_LENGTH + AX_WRITE_DATA + AX_BAUD_RATE + Baud_Rate))&0xFF;

	switchCom(Direction_Pin,Tx_MODE);
	sendData(AX_START);
	sendData(AX_START);
	sendData(ID);
	sendData(AX_BD_LENGTH);
	sendData(AX_WRITE_DATA);
	sendData(AX_BAUD_RATE);
	sendData(Baud_Rate);
	sendData(Checksum);
	delayus(TX_DELAY_TIME);
	switchCom(Direction_Pin,Rx_MODE);
	delayus(RX_DELAY_TIME);

	return (read_error());
}

int DynamixelAPI::move(unsigned char ID, int Position) {
	char Position_H,Position_L;
	Position_H = Position >> 8;
	Position_L = Position;
	Checksum = (~(ID + AX_GOAL_LENGTH + AX_WRITE_DATA + AX_GOAL_POSITION_L + Position_L + Position_H))&0xFF;

	switchCom(Direction_Pin,Tx_MODE);
	sendData(AX_START);
	sendData(AX_START);
	sendData(ID);
	sendData(AX_GOAL_LENGTH);
	sendData(AX_WRITE_DATA);
	sendData(AX_GOAL_POSITION_L);
	sendData(Position_L);
	sendData(Position_H);
	sendData(Checksum);
	delayus(TX_DELAY_TIME);
	switchCom(Direction_Pin, Rx_MODE);
	delayus(RX_DELAY_TIME);

	return (read_error());
}

int DynamixelAPI::moveSpeed(unsigned char ID, int Position, int Speed) {
	char Position_H,Position_L,Speed_H,Speed_L;
	Position_H = Position >> 8;
	Position_L = Position;
	Speed_H = Speed >> 8;
	Speed_L = Speed;
	Checksum = (~(ID + AX_GOAL_SP_LENGTH + AX_WRITE_DATA + AX_GOAL_POSITION_L + Position_L + Position_H + Speed_L + Speed_H))&0xFF;

	switchCom(Direction_Pin,Tx_MODE);
	sendData(AX_START);
	sendData(AX_START);
	sendData(ID);
	sendData(AX_GOAL_SP_LENGTH);
	sendData(AX_WRITE_DATA);
	sendData(AX_GOAL_POSITION_L);
	sendData(Position_L);
	sendData(Position_H);
	sendData(Speed_L);
	sendData(Speed_H);
	sendData(Checksum);
	delayus(TX_DELAY_TIME);
	switchCom(Direction_Pin,Rx_MODE);
	delayus(RX_DELAY_TIME);

	return (read_error());
}

int DynamixelAPI::setEndless(unsigned char ID, bool Status) {
	if(Status) {	
		char AX_CCW_AL_LT = 0;
		Checksum = (~(ID + AX_GOAL_LENGTH + AX_WRITE_DATA + AX_CCW_ANGLE_LIMIT_L))&0xFF;

		switchCom(Direction_Pin,Tx_MODE);
		sendData(AX_START);
		sendData(AX_START);
		sendData(ID);
		sendData(AX_GOAL_LENGTH);
		sendData(AX_WRITE_DATA);
		sendData(AX_CCW_ANGLE_LIMIT_L );
		sendData(AX_CCW_AL_LT);
		sendData(AX_CCW_AL_LT);
		sendData(Checksum);
		delayus(TX_DELAY_TIME);
		switchCom(Direction_Pin,Rx_MODE);
		delayus(RX_DELAY_TIME);

		return(read_error());
	} else {
		turn(ID,0,0);
		Checksum = (~(ID + AX_GOAL_LENGTH + AX_WRITE_DATA + AX_CCW_ANGLE_LIMIT_L + AX_CCW_AL_L + AX_CCW_AL_H))&0xFF;

		switchCom(Direction_Pin,Tx_MODE);
		sendData(AX_START);
		sendData(AX_START);
		sendData(ID);
		sendData(AX_GOAL_LENGTH);
		sendData(AX_WRITE_DATA);
		sendData(AX_CCW_ANGLE_LIMIT_L);
		sendData(AX_CCW_AL_L);
		sendData(AX_CCW_AL_H);
		sendData(Checksum);
		delayus(TX_DELAY_TIME);
		switchCom(Direction_Pin,Rx_MODE);
		delayus(RX_DELAY_TIME);

		return (read_error());
	}
}

int DynamixelAPI::turn(unsigned char ID, bool SIDE, int Speed) {
	if(SIDE == 0) {
		char Speed_H,Speed_L;
		Speed_H = Speed >> 8;
		Speed_L = Speed;
		Checksum = (~(ID + AX_SPEED_LENGTH + AX_WRITE_DATA + AX_GOAL_SPEED_L + Speed_L + Speed_H))&0xFF;

		switchCom(Direction_Pin,Tx_MODE);
		sendData(AX_START);
		sendData(AX_START);
		sendData(ID);
		sendData(AX_SPEED_LENGTH);
		sendData(AX_WRITE_DATA);
		sendData(AX_GOAL_SPEED_L);
		sendData(Speed_L);
		sendData(Speed_H);
		sendData(Checksum);
		delayus(TX_DELAY_TIME);
		switchCom(Direction_Pin,Rx_MODE);
		delayus(RX_DELAY_TIME);

		return(read_error());
	} else {
		char Speed_H,Speed_L;
		Speed_H = (Speed >> 8) + 4;
		Speed_L = Speed;
		Checksum = (~(ID + AX_SPEED_LENGTH + AX_WRITE_DATA + AX_GOAL_SPEED_L + Speed_L + Speed_H))&0xFF;

		switchCom(Direction_Pin,Tx_MODE);
		sendData(AX_START);
		sendData(AX_START);
		sendData(ID);
		sendData(AX_SPEED_LENGTH);
		sendData(AX_WRITE_DATA);
		sendData(AX_GOAL_SPEED_L);
		sendData(Speed_L);
		sendData(Speed_H);
		sendData(Checksum);
		delayus(TX_DELAY_TIME);
		switchCom(Direction_Pin,Rx_MODE);
		delayus(RX_DELAY_TIME);

		return(read_error());
	}
}

int DynamixelAPI::moveRW(unsigned char ID, int Position) {
	char Position_H,Position_L;
	Position_H = Position >> 8;
	Position_L = Position;
	Checksum = (~(ID + AX_GOAL_LENGTH + AX_REG_WRITE + AX_GOAL_POSITION_L + Position_L + Position_H))&0xFF;

	switchCom(Direction_Pin,Tx_MODE);
	sendData(AX_START);
	sendData(AX_START);
	sendData(ID);
	sendData(AX_GOAL_LENGTH);
	sendData(AX_REG_WRITE);
	sendData(AX_GOAL_POSITION_L);
	sendData(Position_L);
	sendData(Position_H);
	sendData(Checksum);
	delayus(TX_DELAY_TIME);
	switchCom(Direction_Pin,Rx_MODE);
	delayus(RX_DELAY_TIME);

	return (read_error());
}

int DynamixelAPI::moveSpeedRW(unsigned char ID, int Position, int Speed) {
	char Position_H,Position_L,Speed_H,Speed_L;
	Position_H = Position >> 8;
	Position_L = Position;
	Speed_H = Speed >> 8;
	Speed_L = Speed;
	Checksum = (~(ID + AX_GOAL_SP_LENGTH + AX_REG_WRITE + AX_GOAL_POSITION_L + Position_L + Position_H + Speed_L + Speed_H))&0xFF;

	switchCom(Direction_Pin,Tx_MODE);
	sendData(AX_START);
	sendData(AX_START);
	sendData(ID);
	sendData(AX_GOAL_SP_LENGTH);
	sendData(AX_REG_WRITE);
	sendData(AX_GOAL_POSITION_L);
	sendData(Position_L);
	sendData(Position_H);
	sendData(Speed_L);
	sendData(Speed_H);
	sendData(Checksum);
	delayus(TX_DELAY_TIME);
	switchCom(Direction_Pin,Rx_MODE);
	delayus(RX_DELAY_TIME);

	return (read_error());
}

void DynamixelAPI::action() {
	switchCom(Direction_Pin,Tx_MODE);
	sendData(AX_START);
	sendData(AX_START);
	sendData(BROADCAST_ID);
	sendData(AX_ACTION_LENGTH);
	sendData(AX_ACTION);
	sendData(AX_ACTION_CHECKSUM);
	delayus(TX_DELAY_TIME);
	switchCom(Direction_Pin,Rx_MODE);
	delayus(RX_DELAY_TIME);
}

int DynamixelAPI::torqueStatus( unsigned char ID, bool Status) {
	Checksum = (~(ID + AX_TORQUE_LENGTH + AX_WRITE_DATA + AX_TORQUE_ENABLE + Status))&0xFF;

	switchCom(Direction_Pin,Tx_MODE);
	sendData(AX_START);
	sendData(AX_START);
	sendData(ID);
	sendData(AX_TORQUE_LENGTH);
	sendData(AX_WRITE_DATA);
	sendData(AX_TORQUE_ENABLE);
	sendData(Status);
	sendData(Checksum);
	delayus(TX_DELAY_TIME);
	switchCom(Direction_Pin,Rx_MODE);
	delayus(RX_DELAY_TIME);

	return (read_error());
}

int DynamixelAPI::ledStatus(unsigned char ID, bool Status) {
	Checksum = (~(ID + AX_LED_LENGTH + AX_WRITE_DATA + AX_LED + Status))&0xFF;

	switchCom(Direction_Pin,Tx_MODE);
	sendData(AX_START);
	sendData(AX_START);
	sendData(ID);
	sendData(AX_LED_LENGTH);
	sendData(AX_WRITE_DATA);
	sendData(AX_LED);
	sendData(Status);
	sendData(Checksum);
	delayus(TX_DELAY_TIME);
	switchCom(Direction_Pin,Rx_MODE);
	delayus(RX_DELAY_TIME);

	return (read_error());
}

int DynamixelAPI::readTemperature(unsigned char ID) {
	Checksum = (~(ID + AX_TEM_LENGTH  + AX_READ_DATA + AX_PRESENT_TEMPERATURE + AX_BYTE_READ))&0xFF;

	switchCom(Direction_Pin,Tx_MODE);
	sendData(AX_START);
	sendData(AX_START);
	sendData(ID);
	sendData(AX_TEM_LENGTH);
	sendData(AX_READ_DATA);
	sendData(AX_PRESENT_TEMPERATURE);
	sendData(AX_BYTE_READ);
	sendData(Checksum);
	delayus(TX_DELAY_TIME);
	switchCom(Direction_Pin,Rx_MODE);
	delayus(RX_DELAY_TIME);

	Temperature_Byte = -1;
	Time_Counter = 0;

	while((availableData(fd) > 0) && (Time_Counter < TIME_OUT)){
		Incoming_Byte1 = readData();
		Incoming_Byte2 = readData();
		if((Incoming_Byte1 == 255) && (Incoming_Byte2 == 255)){
			readData();								// Start Bytes
			readData();								// Ax-12 ID
			readData();								// Length
			if((Error_Byte = readData()) != 0)		// Error
				return (Error_Byte*(-1));

			Temperature_Byte = readData();			// Temperature
		}

		Time_Counter++;
	}
	
	return (Temperature_Byte);
}

int DynamixelAPI::readPosition(unsigned char ID) {
	Checksum = (~(ID + AX_POS_LENGTH  + AX_READ_DATA + AX_PRESENT_POSITION_L + AX_BYTE_READ_POS))&0xFF;

	switchCom(Direction_Pin,Tx_MODE);
	sendData(AX_START);
	sendData(AX_START);
	sendData(ID);
	sendData(AX_POS_LENGTH);
	sendData(AX_READ_DATA);
	sendData(AX_PRESENT_POSITION_L);
	sendData(AX_BYTE_READ_POS);
	sendData(Checksum);
	delayus(TX_DELAY_TIME);
	switchCom(Direction_Pin,Rx_MODE);
	delayus(RX_DELAY_TIME);

	Position_Long_Byte = -1;
	Time_Counter = 0;
	
	while((availableData(fd) > 0) && (Time_Counter < TIME_OUT)) {
		Incoming_Byte1 = readData();
		Incoming_Byte2 = readData();
		if((Incoming_Byte1 == 255) && (Incoming_Byte2 == 255)) {
			readData();								// Start Bytes
			readData();								// Ax-12 ID
			readData();								// Length
			if((Error_Byte = readData()) != 0)		// Error
				return (Error_Byte*(-1));

			Position_Low_Byte = readData();			// Position Bytes
			Position_High_Byte = readData();
			Position_Long_Byte = Position_High_Byte << 8;
			Position_Long_Byte = Position_Long_Byte + Position_Low_Byte;

			break;
		}

		Time_Counter++;
	}

	return (Position_Long_Byte);		// Returns the read position
}

int DynamixelAPI::readVoltage(unsigned char ID) { 
	Checksum = (~(ID + AX_VOLT_LENGTH  + AX_READ_DATA + AX_PRESENT_VOLTAGE + AX_BYTE_READ))&0xFF;

	switchCom(Direction_Pin,Tx_MODE);
	sendData(AX_START);
	sendData(AX_START);
	sendData(ID);
	sendData(AX_VOLT_LENGTH);
	sendData(AX_READ_DATA);
	sendData(AX_PRESENT_VOLTAGE);
	sendData(AX_BYTE_READ);
	sendData(Checksum);
	delayus(TX_DELAY_TIME);
	switchCom(Direction_Pin,Rx_MODE);
	delayus(RX_DELAY_TIME);

	Voltage_Byte = -1;
	Time_Counter = 0;

	while((availableData(fd) > 0) && (Time_Counter < TIME_OUT)) {
		Incoming_Byte1 = readData();
		Incoming_Byte2 = readData();
		if((Incoming_Byte1 == 255) && (Incoming_Byte2 == 255)){
			readData();								// Start Bytes
			readData();								// Ax-12 ID
			readData();								// Length
			if((Error_Byte = readData()) != 0)		// Error
				return (Error_Byte*(-1));
			Voltage_Byte = readData();				// Voltage
		}

		Time_Counter++;
	}

	return (Voltage_Byte);			// Returns the read Voltage
}

int DynamixelAPI::setTempLimit(unsigned char ID, unsigned char Temperature) {
	Checksum = (~(ID + AX_TL_LENGTH +AX_WRITE_DATA+ AX_LIMIT_TEMPERATURE + Temperature))&0xFF;

	switchCom(Direction_Pin,Tx_MODE);
	sendData(AX_START);
	sendData(AX_START);
	sendData(ID);
	sendData(AX_TL_LENGTH);
	sendData(AX_WRITE_DATA);
	sendData(AX_LIMIT_TEMPERATURE);
	sendData(Temperature);
	sendData(Checksum);
	delayus(TX_DELAY_TIME);
	switchCom(Direction_Pin,Rx_MODE);
	delayus(RX_DELAY_TIME);

	return (read_error());
}

int DynamixelAPI::setVoltageLimit(unsigned char ID, unsigned char DVoltage, unsigned char UVoltage) {
	Checksum = (~(ID + AX_VL_LENGTH +AX_WRITE_DATA+ AX_DOWN_LIMIT_VOLTAGE + DVoltage + UVoltage))&0xFF;

	switchCom(Direction_Pin,Tx_MODE);
	sendData(AX_START);
	sendData(AX_START);
	sendData(ID);
	sendData(AX_VL_LENGTH);
	sendData(AX_WRITE_DATA);
	sendData(AX_DOWN_LIMIT_VOLTAGE);
	sendData(DVoltage);
	sendData(UVoltage);
	sendData(Checksum);
	delayus(TX_DELAY_TIME);
	switchCom(Direction_Pin,Rx_MODE);
	delayus(RX_DELAY_TIME);

	return (read_error());
}

int DynamixelAPI::setAngleLimit(unsigned char ID, int CWLimit, int CCWLimit) {
	char CW_H,CW_L,CCW_H,CCW_L;
	CW_H = CWLimit >> 8;
	CW_L = CWLimit;
	CCW_H = CCWLimit >> 8;
	CCW_L = CCWLimit;
	Checksum = (~(ID + AX_VL_LENGTH +AX_WRITE_DATA+ AX_CW_ANGLE_LIMIT_L + CW_H + CW_L + AX_CCW_ANGLE_LIMIT_L + CCW_H + CCW_L))&0xFF;

	switchCom(Direction_Pin,Tx_MODE);
	sendData(AX_START);
	sendData(AX_START);
	sendData(ID);
	sendData(AX_CCW_CW_LENGTH);
	sendData(AX_WRITE_DATA);
	sendData(AX_CW_ANGLE_LIMIT_L);
	sendData(CW_L);
	sendData(CW_H);
	sendData(AX_CCW_ANGLE_LIMIT_L);
	sendData(CCW_L);
	sendData(CCW_H);
	sendData(Checksum);
	delayus(TX_DELAY_TIME);
	switchCom(Direction_Pin,Rx_MODE);
	delayus(RX_DELAY_TIME);

	return (read_error());
}

int DynamixelAPI::setMaxTorque(unsigned char ID, int MaxTorque) {
	char MaxTorque_H,MaxTorque_L;
	MaxTorque_H = MaxTorque >> 8;
	MaxTorque_L = MaxTorque;
	Checksum = (~(ID + AX_MT_LENGTH + AX_WRITE_DATA + AX_MAX_TORQUE_L + MaxTorque_L + MaxTorque_H))&0xFF;

	switchCom(Direction_Pin,Tx_MODE);
	sendData(AX_START);
	sendData(AX_START);
	sendData(ID);
	sendData(AX_MT_LENGTH);
	sendData(AX_WRITE_DATA);
	sendData(AX_MAX_TORQUE_L);
	sendData(MaxTorque_L);
	sendData(MaxTorque_H);
	sendData(Checksum);
	delayus(TX_DELAY_TIME);
	switchCom(Direction_Pin,Rx_MODE);
	delayus(RX_DELAY_TIME);

	return (read_error());
}

int DynamixelAPI::setSRL(unsigned char ID, unsigned char SRL) {
	Checksum = (~(ID + AX_SRL_LENGTH + AX_WRITE_DATA + AX_RETURN_LEVEL + SRL))&0xFF;

	switchCom(Direction_Pin,Tx_MODE);
	sendData(AX_START);
	sendData(AX_START);
	sendData(ID);
	sendData(AX_SRL_LENGTH);
	sendData(AX_WRITE_DATA);
	sendData(AX_RETURN_LEVEL);
	sendData(SRL);
	sendData(Checksum);
	delayus(TX_DELAY_TIME);
	switchCom(Direction_Pin,Rx_MODE);
	delayus(RX_DELAY_TIME);

	return (read_error());
}

int DynamixelAPI::setRDT(unsigned char ID, unsigned char RDT) {
	Checksum = (~(ID + AX_RDT_LENGTH + AX_WRITE_DATA + AX_RETURN_DELAY_TIME + (RDT/2)))&0xFF;

	switchCom(Direction_Pin,Tx_MODE);
	sendData(AX_START);
	sendData(AX_START);
	sendData(ID);
	sendData(AX_RDT_LENGTH);
	sendData(AX_WRITE_DATA);
	sendData(AX_RETURN_DELAY_TIME);
	sendData((RDT/2));
	sendData(Checksum);
	delayus(TX_DELAY_TIME);
	switchCom(Direction_Pin,Rx_MODE);
	delayus(RX_DELAY_TIME);

	return (read_error());
}

int DynamixelAPI::setLEDAlarm(unsigned char ID, unsigned char LEDAlarm) {
	Checksum = (~(ID + AX_LEDALARM_LENGTH + AX_WRITE_DATA + AX_ALARM_LED + LEDAlarm))&0xFF;

	switchCom(Direction_Pin,Tx_MODE);
	sendData(AX_START);
	sendData(AX_START);
	sendData(ID);
	sendData(AX_LEDALARM_LENGTH);
	sendData(AX_WRITE_DATA);
	sendData(AX_ALARM_LED);
	sendData(LEDAlarm);
	sendData(Checksum);
	delayus(TX_DELAY_TIME);
	switchCom(Direction_Pin,Rx_MODE);
	delayus(RX_DELAY_TIME);

	return (read_error());
}

int DynamixelAPI::setShutdownAlarm(unsigned char ID, unsigned char SALARM) {
	Checksum = (~(ID + AX_SALARM_LENGTH + AX_ALARM_SHUTDOWN + AX_ALARM_LED + SALARM))&0xFF;

	switchCom(Direction_Pin,Tx_MODE);
	sendData(AX_START);
	sendData(AX_START);
	sendData(ID);
	sendData(AX_SALARM_LENGTH);
	sendData(AX_WRITE_DATA);
	sendData(AX_ALARM_SHUTDOWN);
	sendData(SALARM);
	sendData(Checksum);
	delayus(TX_DELAY_TIME);
	switchCom(Direction_Pin,Rx_MODE);
	delayus(RX_DELAY_TIME);

	return (read_error());
}

int DynamixelAPI::setCMargin(unsigned char ID, unsigned char CWCMargin, unsigned char CCWCMargin) {
	Checksum = (~(ID + AX_CM_LENGTH +AX_WRITE_DATA+ AX_CW_COMPLIANCE_MARGIN + CWCMargin + AX_CCW_COMPLIANCE_MARGIN + CCWCMargin))&0xFF;

	switchCom(Direction_Pin,Tx_MODE);
	sendData(AX_START);
	sendData(AX_START);
	sendData(ID);
	sendData(AX_CM_LENGTH);
	sendData(AX_WRITE_DATA);
	sendData(AX_CW_COMPLIANCE_MARGIN);
	sendData(CWCMargin);
	sendData(AX_CCW_COMPLIANCE_MARGIN);
	sendData(CCWCMargin);
	sendData(Checksum);
	delayus(TX_DELAY_TIME);
	switchCom(Direction_Pin,Rx_MODE);
	delayus(RX_DELAY_TIME);

	return (read_error());
}

int DynamixelAPI::setCSlope(unsigned char ID, unsigned char CWCSlope, unsigned char CCWCSlope) {
	Checksum = (~(ID + AX_CS_LENGTH +AX_WRITE_DATA+ AX_CW_COMPLIANCE_SLOPE + CWCSlope + AX_CCW_COMPLIANCE_SLOPE + CCWCSlope))&0xFF;

	switchCom(Direction_Pin,Tx_MODE);
	sendData(AX_START);
	sendData(AX_START);
	sendData(ID);
	sendData(AX_CS_LENGTH);
	sendData(AX_WRITE_DATA);
	sendData(AX_CW_COMPLIANCE_SLOPE);
	sendData(CWCSlope);
	sendData(AX_CCW_COMPLIANCE_SLOPE);
	sendData(CCWCSlope);
	sendData(Checksum);
	delayus(TX_DELAY_TIME);
	switchCom(Direction_Pin,Rx_MODE);
	delayus(RX_DELAY_TIME);

	return (read_error());
}

int DynamixelAPI::setPunch(unsigned char ID, int Punch) {
	char Punch_H,Punch_L;
	Punch_H = Punch >> 8;
	Punch_L = Punch;
	Checksum = (~(ID + AX_PUNCH_LENGTH + AX_WRITE_DATA + AX_PUNCH_L + Punch_L + Punch_H))&0xFF;

	switchCom(Direction_Pin,Tx_MODE);
	sendData(AX_START);
	sendData(AX_START);
	sendData(ID);
	sendData(AX_PUNCH_LENGTH);
	sendData(AX_WRITE_DATA);
	sendData(AX_PUNCH_L);
	sendData(Punch_L);
	sendData(Punch_H);
	sendData(Checksum);
	delayus(TX_DELAY_TIME);
	switchCom(Direction_Pin,Rx_MODE);
	delayus(RX_DELAY_TIME);

	return (read_error());
}

int DynamixelAPI::moving(unsigned char ID) {
	Checksum = (~(ID + AX_MOVING_LENGTH  + AX_READ_DATA + AX_MOVING + AX_BYTE_READ))&0xFF;

	switchCom(Direction_Pin,Tx_MODE);
	sendData(AX_START);
	sendData(AX_START);
	sendData(ID);
	sendData(AX_MOVING_LENGTH);
	sendData(AX_READ_DATA);
	sendData(AX_MOVING);
	sendData(AX_BYTE_READ);
	sendData(Checksum);
	delayus(TX_DELAY_TIME);
	switchCom(Direction_Pin,Rx_MODE);
	delayus(RX_DELAY_TIME);

	Moving_Byte = -1;
	Time_Counter = 0;

	while((availableData(fd) > 0) && (Time_Counter < TIME_OUT)) {
		Incoming_Byte1 = readData();
		Incoming_Byte2 = readData();
		if((Incoming_Byte1 == 255) && (Incoming_Byte2 == 255)) {
			readData();								// Start Bytes
			readData();								// Ax-12 ID
			readData();								// Length
			if((Error_Byte = readData()) != 0)		// Error
				return (Error_Byte*(-1));
			Moving_Byte = readData();				// Temperature
		}

		Time_Counter++;
	}

	return (Moving_Byte);			// Returns the read temperature
}

int DynamixelAPI::lockRegister(unsigned char ID) {
	Checksum = (~(ID + AX_LR_LENGTH + AX_WRITE_DATA + AX_LOCK + LOCK))&0xFF;

	switchCom(Direction_Pin,Tx_MODE);
	sendData(AX_START);
	sendData(AX_START);
	sendData(ID);
	sendData(AX_LR_LENGTH);
	sendData(AX_WRITE_DATA);
	sendData(AX_LOCK);
	sendData(LOCK);
	sendData(Checksum);
	delayus(TX_DELAY_TIME);
	switchCom(Direction_Pin,Rx_MODE);
	delayus(RX_DELAY_TIME);

	return (read_error());
}

int DynamixelAPI::RWStatus(unsigned char ID) {
	Checksum = (~(ID + AX_RWS_LENGTH  + AX_READ_DATA + AX_REGISTERED_INSTRUCTION + AX_BYTE_READ))&0xFF;

	switchCom(Direction_Pin,Tx_MODE);
	sendData(AX_START);
	sendData(AX_START);
	sendData(ID);
	sendData(AX_RWS_LENGTH);
	sendData(AX_READ_DATA);
	sendData(AX_REGISTERED_INSTRUCTION);
	sendData(AX_BYTE_READ);
	sendData(Checksum);
	delayus(TX_DELAY_TIME);
	switchCom(Direction_Pin,Rx_MODE);
	delayus(RX_DELAY_TIME);

	RWS_Byte = -1;
	Time_Counter = 0;

	while ((availableData(fd) > 0) && (Time_Counter < TIME_OUT)) {
		Incoming_Byte1 = readData();
		Incoming_Byte2 = readData();
		if((Incoming_Byte1 == 255) && (Incoming_Byte2 == 255)) {
			readData();								// Start Bytes
			readData();								// Ax-12 ID
			readData();								// Length
			if((Error_Byte = readData()) != 0)		// Error
				return (Error_Byte*(-1));
			RWS_Byte = readData();					// Temperature
		}

		Time_Counter++;
	}

	return (RWS_Byte);			// Returns the read temperature
}

int DynamixelAPI::readSpeed(unsigned char ID) {
	Checksum = (~(ID + AX_POS_LENGTH  + AX_READ_DATA + AX_PRESENT_SPEED_L + AX_BYTE_READ_POS))&0xFF;

	switchCom(Direction_Pin,Tx_MODE);
	sendData(AX_START);
	sendData(AX_START);
	sendData(ID);
	sendData(AX_POS_LENGTH);
	sendData(AX_READ_DATA);
	sendData(AX_PRESENT_SPEED_L);
	sendData(AX_BYTE_READ_POS);
	sendData(Checksum);
	delayus(TX_DELAY_TIME);
	switchCom(Direction_Pin,Rx_MODE);
	delayus(RX_DELAY_TIME);

	Speed_Long_Byte = -1;
	Time_Counter = 0;

	while((availableData(fd) > 0) && (Time_Counter < TIME_OUT)) {
		Incoming_Byte1 = readData();
		Incoming_Byte2 = readData();
		if((Incoming_Byte1 == 255) && (Incoming_Byte2 == 255)) {
			readData();								// Start Bytes
			readData();								// Ax-12 ID
			readData();								// Length
			if((Error_Byte = readData()) != 0)		// Error
				return (Error_Byte*(-1));
			
			Speed_Low_Byte = readData();			// Position Bytes
			Speed_High_Byte = readData();
			Speed_Long_Byte = Speed_High_Byte << 8;
			Speed_Long_Byte = Speed_Long_Byte + Speed_Low_Byte;
		}

		Time_Counter++;
	}

	return (Speed_Long_Byte);		// Returns the read position
}

int DynamixelAPI::readLoad(unsigned char ID) {
	Checksum = (~(ID + AX_POS_LENGTH  + AX_READ_DATA + AX_PRESENT_LOAD_L + AX_BYTE_READ_POS))&0xFF;

	switchCom(Direction_Pin,Tx_MODE);
	sendData(AX_START);
	sendData(AX_START);
	sendData(ID);
	sendData(AX_POS_LENGTH);
	sendData(AX_READ_DATA);
	sendData(AX_PRESENT_LOAD_L);
	sendData(AX_BYTE_READ_POS);
	sendData(Checksum);
	delayus(TX_DELAY_TIME);
	switchCom(Direction_Pin,Rx_MODE);
	delayus(RX_DELAY_TIME);

	Load_Long_Byte = -1;
	Time_Counter = 0;

	while((availableData(fd) > 0) && (Time_Counter < TIME_OUT)) {
		Incoming_Byte1 = readData();
		Incoming_Byte2 = readData();
		if((Incoming_Byte1 == 255) && (Incoming_Byte2 == 255)) {
			readData();								// Start Bytes
			readData();								// Ax-12 ID
			readData();								// Length
			if((Error_Byte = readData()) != 0)		// Error
				return (Error_Byte*(-1));
			
			Load_Low_Byte = readData();				// Position Bytes
			Load_High_Byte = readData();
			Load_Long_Byte = Load_High_Byte << 8; 
			Load_Long_Byte = Load_Long_Byte + Load_Low_Byte;
		}

		Time_Counter++;
	}

	return (Load_Long_Byte);		// Returns the read position
}

void DynamixelAPI::sendData(unsigned char packet) {
	int wlen;
	wlen = write(fd, &packet, 1);
	tcdrain(fd);
}

int DynamixelAPI::readData() {
	read(fd, buf, 1);
	// printf(" %d\n", buf[0]);
	// printf(" 0x%x\n", buf[0]);

	return buf[0];
}