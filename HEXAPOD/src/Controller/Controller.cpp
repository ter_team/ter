/*
 * File: /Users/thibautmorant/Desktop/UPSSITECH/M1/S1/TER/PROJECT/ter/HEXAPOD/src/Controller/Controller.cpp
 * Created Date: Tuesday November 27th 2018
 * Author: MORANT Thibaut
 * 
 * -----
 * 
 * Last Modified: Wed Nov 28 2018
 * Modified By: MORANT Thibaut
 * 
 * -----
 * 
 * HEXAPOD
 * Copyright (C) 2018  MORANT Thibaut
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * -----
 * 
 * HISTORY:
 */


#include "../../include/Controller/Controller.h"

using namespace std;


Controller::Controller(string path) {
	cout << "--- CREATE CONTROLLER ---" << endl;

	if((device = open(path.c_str(), O_RDONLY)) == 0) {
		cout << "ERROR: Controller not connected." << endl;
		exit(1);
	}

	getJSIO();
	initMAP_AB();

	run  = true;
}

Controller::~Controller() {
	cout << "--- DELETE CONTROLLER ---" << endl;
}

void Controller::getJSIO() {
	ioctl(device, JSIOCGAXES, &axes);
	ioctl(device, JSIOCGBUTTONS, &buttons);
}

void Controller::initMAP_AB() {
	for(uint8_t i(0); i < unsigned(axes); i++)
		axis[unsigned(i)] = 0;

	for(uint8_t i(0); i < unsigned(buttons); i++)
		button[unsigned(i)] = false;
}

void Controller::readDevice() {
	while(run) {

		if(read(device, &js, sizeof(struct js_event)) != sizeof(struct js_event)) {
			cerr << "ERROR: during reading." << endl;

			exit(1);
		}

		mtx.lock();

		switch(js.type & ~JS_EVENT_INIT) {
			case JS_EVENT_BUTTON:
				button[js.number] = js.value;

				break;
			case JS_EVENT_AXIS:
				axis[js.number] = js.value;

				break;
		}


		// printf("\r");

		// if (axes) {
		// 	printf("Axes: ");
		// 	for (int i = 0; i < axes; i++)
		// 		printf("%2d:%6d ", i, axis[i]);
		// }

		// if (buttons) {
		// 	printf("Buttons: ");
		// 	for (int i = 0; i < buttons; i++)
		// 		printf("%2d:%s ", i, button[i] ? "on " : "off");
		// }

		// fflush(stdout);	

		
		checkEvent();

		mtx.unlock();
	}
}

void Controller::Start(Hexapod *hexapod) {
	thread th(&Controller::readDevice, this);

	Control(hexapod);

	th.join();
}

void Controller::checkEvent() {
		if(button[PS])
			run = false;
}

void Controller::Control(Hexapod *hexapod) {
	while(run) {
		if(button[UP_])
			cout << "UP" << endl;

		if(button[DOWN_])
			cout << "DOWN" << endl;

		if(button[LEFT_])
			cout << "LEFT" << endl;

		if(button[RIGHT_])
			cout << "RIGHT" << endl;

		hexapod->holonomy(-axis[JLy]/128, axis[JLx]/128);
	}
}