/*
 * File: /Users/thibautmorant/Desktop/UPSSITECH/M1/S1/TER/PROJECT/ter/HEXAPOD/src/Structure/Leg.cpp
 * Created Date: Tuesday November 27th 2018
 * Author: MORANT Thibaut
 * 
 * -----
 * 
 * Last Modified: Wed Nov 28 2018
 * Modified By: MORANT Thibaut
 * 
 * -----
 * 
 * HEXAPOD
 * Copyright (C) 2018  MORANT Thibaut
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * -----
 * 
 * HISTORY:
 */


#include "../../include/Structure/Leg.h"

using namespace std;


Leg::Leg(float *value, int ID) {
	cout << "--- ADD LEGS ---" << endl;

	legPart_1 = value[0];
	legPart_2 = value[1];
	legPart_3 = value[2];
	thetaCorrection_1 = value[3];
	thetaCorrection_2 = value[4];
	thetaCorrection_3 = value[5];
	this->x = value[6];
	this->y = value[7];
	this->ID = ID;

	initT0X();

	for(int i(ID); i < 30; i += 10)
		dynamixels.push_back(new Dynamixel(i));
}

Leg::~Leg() {
	cout << "--- DELETE LEGS ---" << endl;
	
	for(int i(0); i < dynamixels.size(); i++) 
		delete dynamixels[i];

	delete []dynamixels;

	dynamixels.clear();
}

void Leg::initT0X() {
	T0X = new float *[4];

	for(int i(0); i < 4; i++)
		T0X[i] = new float[4];

	T0X[2][0] = T0X[2][1] = T0X[0][2] = T0X[1][2] = T0X[0][3] = T0X[1][3] = T0X[2][3] = 0;
	T0X[2][2] = T0X[3][3] = 1;

	T0X[0][0] = x / (sqrt(x*x + y*y));
	T0X[1][0] = -y / (sqrt(x*x + y*y));
	T0X[0][1] = x / (sqrt(x*x + y*y));
	T0X[1][1] = y / (sqrt(x*x + y*y));

	T0X[3][0] = x;
	T0X[3][1] = y;
}

void Leg::move(float x, float y, float z) {
	float c = (x/(sqrt(this->x*this->x + this->y*this->y)));
	float s = (y/(sqrt(this->x*this->x + this->y*this->y)));

	float coorleg1 = (x - this->x)*c + (y - this->y)*s;
	float coorleg2 = -(x - this->x)*s + (y - this->y)*c;
	float coorleg3 = z;

	// kinematic->inverseKinematic((x-T0X[3][0])*cos(T0X[0][0]) + (y-T0X[3][1])*sin(T0X[1][0]), (x-T0X[3][0])*sin(T0X[0][1]) + (y-T0X[3][1])*cos(T0X[1][1]), z, thetaCorrection_1, thetaCorrection_2, thetaCorrection_3, legPart_1, legPart_2, legPart_3);
	
	float *new_angles = kinematic->inverseKinematic(coorleg1, coorleg2, coorleg3, thetaCorrection_1, thetaCorrection_2, thetaCorrection_3, legPart_1, legPart_2, legPart_3);
	
	dynamixels[0]->move(new_angles[0]);
	dynamixels[1]->move(-new_angles[1]);
	dynamixels[2]->move(new_angles[2]);
}

string Leg::toString() const {

	return "\nID: " + to_string(ID) + "\nParameter:\n	legPart_1: " + to_string(legPart_1) + "\n	legPart_2: " + to_string(legPart_2) + "\n	legPart_3: "
			+ to_string(legPart_3) + "\n	thetaCorrection_1: " + to_string(thetaCorrection_1) + "\n	thetaCorrection_2: "
			+ to_string(thetaCorrection_2) + "\n	thetaCorrection_3: " + to_string(thetaCorrection_3)
			+ "\n	x: : " + to_string(x) + "\n	y: : " + to_string(y);
}