/*
 * File: /Users/thibautmorant/Desktop/UPSSITECH/M1/S1/TER/PROJECT/ter/HEXAPOD/src/Structure/Hexapod.cpp
 * Created Date: Tuesday November 27th 2018
 * Author: MORANT Thibaut
 * 
 * -----
 * 
 * Last Modified: Wed Nov 28 2018
 * Modified By: MORANT Thibaut
 * 
 * -----
 * 
 * HEXAPOD
 * Copyright (C) 2018  MORANT Thibaut
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * -----
 * 
 * HISTORY:
 */


#include "../../include/Structure/Hexapod.h"

using namespace std;
using namespace rapidjson;


Hexapod::Hexapod() {
	cout << "--- CREATE HEXAPOD ---" << endl;

	FILE* file = fopen("./JSON/Hexapod.json", "r");

	if(!file)
		getError(0);

	char readBuffer[1554];
	FileReadStream frs(file, readBuffer, sizeof(readBuffer));

	document.ParseStream(frs);

	checkJSON();

	float *value;

	for(int i(0); i < 6; i++) {
		value = assignValue(i);

		legs.push_back(new Leg(value, i+1));
	}

	fclose(file);
	
	initPosition();

	legsParity = false;
}

Hexapod::~Hexapod() {
	cout << "--- DELETE HEXAPOD ---" << endl;
	
	for(int i(0); i < legs.size(); i++)
		delete legs[i];

	delete []dynamixels;

	legs.clear();
}

void Hexapod::checkJSON() {
	string member_1, member_2;
	int i(2);
	int y(2);

	if(!document.IsObject())
		getError(1, "JSON File");

	if(!document.HasMember("HEXAPOD"))
		getError(3, "HEXAPOD");
	if(!document["HEXAPOD"].IsObject())
		getError(1, "HEXAPOD");

	member_1.assign("LEG_1");

	do {
		if(!document["HEXAPOD"].HasMember(member_1.c_str()))
			getError(3, member_1 + ", element of HEXAPOD");
		if(!document["HEXAPOD"][member_1.c_str()].IsObject())
			getError(1, member_1 + ", element of HEXAPOD");

		if(!document["HEXAPOD"][member_1.c_str()].HasMember("X"))
			getError(3, "X, element of " + member_1);
		if(!document["HEXAPOD"][member_1.c_str()].HasMember("Y"))
			getError(3, "Y, element of " + member_1);
		if(!document["HEXAPOD"][member_1.c_str()]["X"].IsFloat())
			getError(2, "X, element of " + member_1);
		if(!document["HEXAPOD"][member_1.c_str()]["Y"].IsFloat())
			getError(2, "Y, element of " + member_1);

		member_2.assign("LEG_P1");

		do {
			if(!document["HEXAPOD"][member_1.c_str()].HasMember(member_2.c_str()))
				getError(3, member_2 + ", element of " + member_1);
			if(!document["HEXAPOD"][member_1.c_str()][member_2.c_str()].IsObject())
				getError(1, member_2 + ", element of " + member_1);

			if(!document["HEXAPOD"][member_1.c_str()][member_2.c_str()].HasMember("LENGTH"))
				getError(3, "LENGTH, element of " + member_2 + " contained in " + member_1 + " structure,");
			if(!document["HEXAPOD"][member_1.c_str()][member_2.c_str()].HasMember("THETA_CORRECTION"))
				getError(3, "THETA_CORRECTION, element of " + member_2 + " contained in " + member_1 + " structure,");
			if(!document["HEXAPOD"][member_1.c_str()][member_2.c_str()]["LENGTH"].IsFloat())
				getError(2, "LENGTH, element of " + member_2 + " contained in " + member_1 + " structure,");
			if(!document["HEXAPOD"][member_1.c_str()][member_2.c_str()]["THETA_CORRECTION"].IsFloat())
				getError(2, "THETA_CORRECTION, element of " + member_2 + " contained in " + member_1 + " structure,");

			member_2.replace(member_2.end()-1, member_2.end(), to_string(y));
			y++;
		} while(y < 5);
		y = 2;

		member_1.replace(member_1.end()-1, member_1.end(), to_string(i));
		i++;
	} while(i < 8);
}

float *Hexapod::assignValue(int ID) {
	string member_1, member_2;
	float *value = new float[8];
	int i(2);

	member_1.assign("LEG_");
	member_1.append(to_string(ID+1));

	member_2.assign("LEG_P1");

	do {
		value[i-2] = document["HEXAPOD"][member_1.c_str()][member_2.c_str()]["LENGTH"].GetFloat();
		value[i+1] = document["HEXAPOD"][member_1.c_str()][member_2.c_str()]["THETA_CORRECTION"].GetFloat();

		member_2.replace(member_2.end()-1, member_2.end(), to_string(i));
		i ++;
	} while(i < 5);

	value[6] = document["HEXAPOD"][member_1.c_str()]["X"].GetFloat();
	value[7] = document["HEXAPOD"][member_1.c_str()]["Y"].GetFloat();


	return value;
}

void Hexapod::holonomy(int x, int y) {
	// cout << "x: " << x << " y: " << y << endl;
}

void Hexapod::getError(int value, string information) {
	switch(value) {
		case 0:
			cout << "ERROR: JSON file not found." << endl;

			break;
		case 1:
			cout << "ERROR: " << information << " is not an Object." << endl;

			break;
		case 2:
			cout << "ERROR: " << information << " is not an Float." << endl;

			break;
		case 3:
			cout << "ERROR: member " << information << " not present in JSON file." << endl;

			break;
		default:

			break;
	}

	exit(1);
}

void Hexapod::initPosition() {
	position = new float *[6];

	for(int i(0); i < 6; i++)
		position[i] = new float[3];


	position[0][0] = -position[2][0] = position[3][0] = -position[5][0] = 254;
	position[0][1] = position[2][1] = -position[3][1] = position[5][1] = 72;

	position[2][0] = position[4][0] = 0;
	position[2][1] = position[4][1] = 0;

	position[0][2] = position[1][2] = position[2][2] = position[3][2] = position[4][2] = position[5][2] = -78;

	// legs[0]->move( , , );
	// legs[1]->move( , , );
	// legs[2]->move( , , );
	// legs[3]->move( , , );
	// legs[4]->move( , , );
	// legs[5]->move( , , );
}

string Hexapod::toString() const {
	string s;

	s.assign("Legs:\n");
	
	for(int i(0); i < legs.size(); i++)
		s.append("leg_" + to_string(i+1) + ": " + legs[i]->toString() + "\n");


	return s;
}