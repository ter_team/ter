# TER #

Development of a Hexapod during an university project.

### LICENSE

|   License     |     Hardware    |   Software      |
| ------------- | :-------------: | :-------------: |
| Title  | [Creatives Commons BY-SA](http://creativecommons.org/licenses/by-sa/4.0/)  |[GPL v3](http://www.gnu.org/licenses/gpl.html)  |
| Logo  | [![Creative Commons BY-SA](https://i.creativecommons.org/l/by-sa/4.0/88x31.png) ](http://creativecommons.org/licenses/by-sa/4.0/)  |[![GPL V3](https://www.gnu.org/graphics/gplv3-88x31.png)](http://www.gnu.org/licenses/gpl.html)  |

* The source code is released as open source software under the GPL v3 license, see the LICENSE file in the project root for the full license text.
* The blender and svg files are licensed under a Creative Commons Attribution-ShareAlike 4.0 International License, see the LICENSE file in the "MODELISATION" folder for the full license text.